Feature: Cart Mount

  Scenario Outline: Verify cart amount is greater than 10000
    Given I am logged
    When I search the following Products, add to Cart and NoAddCoverage
      | Product      | Brand   |
      | Refrigerador | Samsung |
      | Refrigerador | Mabe    |
    Then Cart Total should be greater than <Total>
    Examples:
      | Total      |
      | 10000      |

  Scenario Outline: Verify cart amount is lower than 10000
    Given I am logged
    When I search the following Products, add to Cart and NoAddCoverage
      | Product      | Brand   |
      | Refrigerador | Samsung |
      | Refrigerador | Mabe    |
    Then Cart Total should be lower than <Total>
    Examples:
      | Total      |
      | 10000      |

