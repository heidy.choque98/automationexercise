package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.Hooks;

public class ProductPage {

    @FindBy(id = "add-to-cart-button")
    private WebElement addToCartElement;

    public ProductPage() {
        PageFactory.initElements(Hooks.driver, this);
    }

    public void addToCart()
    {
        addToCartElement.click();
    }
}
