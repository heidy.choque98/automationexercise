package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.junit.Assert;
import steps.Hooks;

public class CartPage {

    @FindBy(xpath = "//span[@id='attachSiNoCoverage-announce']/preceding-sibling::input")
    private WebElement noAddCoverage;

    @FindBy(id = "sw-subtotal-item-count")
    private WebElement subTotalTitle;

    @FindBy(xpath = "//span[@id='sw-subtotal-item-count' ]/following-sibling::span/descendant::span[@class='a-offscreen']")
    private  WebElement subTotalCart;

    public CartPage() {
        PageFactory.initElements(Hooks.driver, this);
    }

    public void noAddCoverage(){
        Hooks.wait.until(ExpectedConditions.visibilityOf(noAddCoverage));
        noAddCoverage.click();
        Hooks.wait.until(ExpectedConditions.visibilityOf(subTotalTitle));
    }

    public boolean isGreaterTotalCart(int totalCart){
        String total = subTotalCart.getAttribute("innerText");
        total = total.substring(1);
        total = total.replaceAll(",", "");
        String[] parts = total.split("\\.");
        String totalString = parts[0]; // 123
        int totalInt = Integer.parseInt(totalString);

        boolean isgreater=false;
        if(totalInt > totalCart)
        {
            isgreater = true;
        }
        else {
            isgreater = false;
        }
        return isgreater;
    }

    public boolean isLowerTotalCart(int totalCart){
        String total = subTotalCart.getAttribute("innerText");
        total = total.substring(1);
        total = total.replaceAll(",", "");
        String[] parts = total.split("\\.");
        String totalString = parts[0]; // 123
        int totalInt = Integer.parseInt(totalString);

        boolean islower=false;
        if(totalInt < totalCart)
        {
            islower = true;
        }
        else {
            islower = false;
        }
        return islower;
    }

    public void verifyTotalCartGreater(int totalCart)
    {
        Assert.assertTrue(isGreaterTotalCart(totalCart));
    }

    public void verifyTotalCartLower(int totalCart)
    {
        Assert.assertTrue(isLowerTotalCart(totalCart));
    }
}
