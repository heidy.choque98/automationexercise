package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.Hooks;

public class HomePage {

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchBox;

    @FindBy(id = "nav-search-submit-button")
    private WebElement searchButton;

    public HomePage() {
        PageFactory.initElements(Hooks.driver, this);
    }

    public void searchText(String text)
    {
        searchBox.sendKeys(text);
        searchButton.click();
    }
}