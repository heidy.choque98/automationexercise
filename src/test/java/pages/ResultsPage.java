package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import steps.Hooks;

public class ResultsPage {

    public static final String samsungBrand = "Samsung";
    public static final String mabeBrand = "Mabe";

    @FindBy(xpath = "(//span[text()='RESULTADOS']/ancestor::div[@class='s-main-slot s-result-list s-search-results sg-row']/div/following-sibling::div[contains(@class,'s-result-item s-asin sg-col')]/descendant::span[contains(text(),'"+samsungBrand+"')])[1]" )
    private WebElement firstSamsungSearchedElement;

    @FindBy(xpath = "(//span[text()='RESULTADOS']/ancestor::div[@class='s-main-slot s-result-list s-search-results sg-row']/div/following-sibling::div[contains(@class,'s-result-item s-asin sg-col')]/descendant::span[contains(text(),'"+mabeBrand+"')])[1]" )
    private WebElement firstMabeSearchedElement;

    public ResultsPage() {
        PageFactory.initElements(Hooks.driver, this);
    }

    public void clickOnFoundElement(String brand)
    {
        if (brand.equals(samsungBrand))
        {
            firstSamsungSearchedElement.click();
        }
        else if (brand.equals(mabeBrand)){
            Hooks.wait.until(ExpectedConditions.visibilityOf(firstMabeSearchedElement));
            firstMabeSearchedElement.click();
        }
    }
}
