package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;

import org.openqa.selenium.WebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import static java.time.temporal.ChronoUnit.SECONDS;

public class Hooks{


    public static WebDriver driver;
    public static WebDriverWait wait;

    private final static String PAGE_URL="https://amazon.com.mx";

    @Before
    public void beforeScenario(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(PAGE_URL);
        driver.manage().window().maximize() ;
        wait =new WebDriverWait(driver, Duration.of(4, SECONDS));
    }

    @After
    public void afterScenario(){
        driver.quit();
    }
}
