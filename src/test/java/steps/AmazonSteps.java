package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.CartPage;
import pages.HomePage;
import pages.ProductPage;
import pages.ResultsPage;

import java.util.List;
import java.util.Map;

public class AmazonSteps {

    @Given("I am logged")

    public void iAmLogged()
    {
        System.out.println("Yes i logged");
    }

    @When("I search the following Products, add to Cart and NoAddCoverage")

    public void iSearch(DataTable table)
    {
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);

        for (Map<String, String> columns : rows) {
            new HomePage().searchText(columns.get("Product"));
            new ResultsPage().clickOnFoundElement(columns.get("Brand"));
            new ProductPage().addToCart();
            new CartPage().noAddCoverage();
        }
    }

    @Then("^Cart Total should be (.*) than (.*)")
    public void cartTotalShouldBeThanTotal(String option, int mount) {
        if(option.equals("greater")) {
            new CartPage().verifyTotalCartGreater(mount);
        }
        else if (option.equals("lower")){
            new CartPage().verifyTotalCartLower(mount);
        }
    }
}
