# Automation Exercise

Simple Automation framework for amazom.com

## Getting Started

### Dependencies and Tools

* Gradle.
* Java.
* Selenium.
* Cucumber.
* PageFactory.

### Installing

* Clone repository.
```
git clone https://gitlab.com/heidy.choque98/automationexercise.git
```

* Add Gradle project.

## Execution

### Using Gradle
* Run
```
gradle test
```
### Using TestRunner
* Go to src/test/java/runner
* Execute TestRunner.java

### Running Features
* Go to src/test/resources/features
* Execute a feature

## Authors

Heidy choque
